#region File Header
// Filename: Result.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Popup
{
    public class Result : MonoBehaviour
    {
        [SerializeField]
        private Button _retry;

        [SerializeField] 
        private GameObject _win;
        [SerializeField]
        private GameObject _lose;

        private void Start()
        {
            _retry.onClick.AddListener(HandleRetry);
            TogglePopup(false);
        }

        private void OnDestroy()
        {
            _retry.onClick.RemoveListener(HandleRetry);
        }

        public void Show(bool isWin)
        {
            _win.SetActive(isWin);    
            _lose.SetActive(!isWin);
            TogglePopup(true);
        }

        private void TogglePopup(bool show)
        {
            gameObject.SetActive(show);
        }

        /// <summary>
        /// reload sample scene.
        /// </summary>
        private void HandleRetry()
        {
            SceneManager.LoadScene(0);
        }
    }
}