#region File Header
// Filename: EnemyScript.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using ArcaneWater.Effects;
using UnityEngine;

namespace ArcanWater.Entity
{
    public class EnemyScript : EntityBase
    {
        [SerializeField]
        private SelectionButton _select;
        [SerializeField]
        private GameObject _selectionRoot;

        /// <inheritdoc />
        public override EntityType Type => EntityType.Enemy;

        private void Start()
        {
            _select.OnButtonClicked += HandleTargetClicked;
        }

        private void OnDestroy()
        {
            _select.OnButtonClicked -= HandleTargetClicked;
        }

        private void HandleTargetClicked()
        {
            Manager.OnTargetClicked(this);
        }
        
        public override void OnTurnStart()
        {
            SetTarget(Manager.GetPlayer());
            base.OnTurnStart();
        }


        /// <inheritdoc />
        public override void OnDeath()
        {
            ToggleEnemyTarget(false);
            base.OnDeath();
        }

        
        /// <summary>
        /// Toggle enemy Selection Button.
        /// </summary>
        /// <param name="enable"></param>
        public void ToggleEnemyTarget(bool enable)
        {
            if (IsDead)
            {
                return;
            }

            _selectionRoot.SetActive(enable);    
            _select.gameObject.SetActive(enable);
        }


        /// <inheritdoc />
        public override void SetTarget(EntityBase target)
        {
            base.SetTarget(target);
            _animator.SetInteger("Attack", 1);
        }
    }
}