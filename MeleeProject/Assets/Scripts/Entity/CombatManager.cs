#region File Header
// Filename: CombatManager.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using System.Collections.Generic;
using System.Linq;
using ArcaneWater.Effects;
using Popup;
using UnityEngine;

namespace ArcanWater.Entity
{
    public class CombatManager : MonoBehaviour
    {
        [SerializeField] 
        private List<EntityBase> _entities;
        [SerializeField] 
        private CameraShake _camera;
        [SerializeField]
        private Result _result;

        private List<EnemyScript> _enemies = new List<EnemyScript>();
        private List<EntityBase> _player = new List<EntityBase>();

        /// <summary>
        /// return current character in turn.
        /// </summary>
        private EntityBase CurrentEntity { get; set; }
        
        /// <summary>
        /// current index of turn
        /// </summary>
        private int CurrentIndex { get; set; }
        
        /// <summary>
        /// return max character in combat.
        /// </summary>
        private int MaxIndex => _entities.Count;
        
        /// <summary>
        /// return true if match already ended.
        /// </summary>
        private bool MatchEnd { get; set; }

        private void Start()
        {
            MatchEnd = false;
            foreach (var entity in _entities)
            {
                entity.Initialize(this);
                if (entity.Type == EntityType.Enemy)
                {
                    var ent = (EnemyScript) entity;
                    ent.ToggleEnemyTarget(false);
                    _enemies.Add(ent);
                }
                else
                {
                    _player.Add(entity);
                }
            }

            CurrentIndex = Random.Range(0, _entities.Count);
            SetEntity(_entities[CurrentIndex]);
        }

        /// <summary>
        /// Invoke to set the next character in turn .
        /// </summary>
        public void OnCombatContinue()
        {
            CheckVictory();
            
            if (MatchEnd)
            {
                return;
            }

            if (CurrentIndex + 1 < MaxIndex)
            {
                CurrentIndex++;
            }
            else
            {
                CurrentIndex = 0;
            }
            SetEntity(_entities[CurrentIndex]);
        }
        
        /// <summary>
        /// Set character in turn and check if entity is dead. Repick if character is dead.
        /// </summary>
        /// <param name="entity"></param>
        private void SetEntity(EntityBase entity)
        {
            if (entity.IsDead)
            {
                    OnCombatContinue();
                return;
            }

            CurrentEntity = entity;
            if (CurrentEntity.Type == EntityType.Player)
            {
                ToggleEnemyTarget(true);
            }
            CurrentEntity.OnTurnStart();
        }


        /// <summary>
        /// Toggle enemy button and indicator UIs.
        /// </summary>
        /// <param name="enable"></param>
        private void ToggleEnemyTarget(bool enable)
        {
            foreach (var enemy in _enemies)
            {
                enemy.ToggleEnemyTarget(enable);
            }
        }
        
        /// <summary>
        /// invoked when an enemy was picked as a target.
        /// </summary>
        /// <param name="entity"></param>
        public void OnTargetClicked(EntityBase entity)
        {
            ToggleEnemyTarget(false);
            CurrentEntity.SetTarget(entity);
        }

        /// <summary>
        /// Get random Player if multiple
        /// </summary>
        /// <returns></returns>
        public EntityBase GetPlayer()
        {
            var alive = _player.Where(item => !item.IsDead).ToList();
            var index = Random.Range(0, alive.Count);
            return alive[index];
        }

        /// <summary>
        /// Invoke to check if match ended.
        /// </summary>
        public void CheckVictory()
        {
            if (MatchEnd)
            {
                return;
            }

            if (_player.All(item => item.IsDead))
            {
                HandleDefeat();
            }
            else if (_enemies.All(item => item.IsDead))
            {
                HandleVictory();
            }
        }

        /// <summary>
        /// Invoke to show result
        /// </summary>
        private void HandleVictory()
        {
            MatchEnd = true;
            _result.Show(true);
        }

        /// <summary>
        /// Invoke to show result
        /// </summary>
        private void HandleDefeat()
        {
            MatchEnd = true;
            _result.Show(false);
        }
        
        /// <summary>
        /// invoke camera shake.
        /// </summary>
        public void Shake()
        {
            _camera.Shake();
        }
    }
}