#region File Header
// Filename: PlayerScript.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

namespace ArcanWater.Entity
{
    public class PlayerScript : EntityBase
    {
        public override EntityType Type => EntityType.Player;

        /// <inheritdoc />
        public override void SetTarget(EntityBase target)
        {
            base.SetTarget(target);
            _animator.SetInteger("Attack", 1);  
        }
    }
}