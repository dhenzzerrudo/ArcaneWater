#region File Header
// Filename: EntityScript.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using ArcaneWater.Effects;
using UnityEngine;

namespace ArcanWater.Entity
{
    public class EntityBase : MonoBehaviour
    {
        [SerializeField] 
        private HpHandler _hpHandler;
        [SerializeField] 
        private DamageEffectBase _damageEffect;
        [SerializeField] 
        private DamageScript _attackDamage;
        [SerializeField] 
        private Transform _hitPosition;
        [SerializeField] 
        protected Animator _animator;
        [SerializeField] 
        protected int MinDamage;
        [SerializeField] 
        protected int MaxDamage;
        [SerializeField] 
        private int _hp;

        private int PendingDamage { get; set; }
        private Vector3 Origin{get; set; }
        
        protected CombatManager Manager { get; private set; }
        protected bool StartMove { get; set; }
        protected Vector3 TargetPosition { get; set; }
        protected EntityBase TargetEntity { get; set; }

        /// <summary>
        /// return character current State.
        /// </summary>
        public EntityState State { get; private set; }
        
        /// <summary>
        /// Return character type.
        /// </summary>
        public virtual EntityType Type { get; }
 
        /// <summary>
        /// Return true if character dies.
        /// </summary>
        public bool IsDead { get; set; }        
        
        /// <summary>
        /// return position where hitting character will move before attacking character.
        /// </summary>
        public Transform HitPosition => _hitPosition;

        public void Initialize(CombatManager manager)
        {
            IsDead = false;
            Manager = manager;
            _hpHandler.Initialize(_hp);
            Origin = transform.parent.position;
        }

        /// <summary>
        /// Triggered on Animation event. Method is Triggered before attacking.
        /// </summary>
        public virtual void OnJump()
        {
            State = EntityState.Attacking;
            TargetPosition = TargetEntity.HitPosition.position;
            StartMove = true;
        }
        

        /// <summary>
        /// Triggered on Animation event. Method is triggered when character is doing attack Animation.
        /// </summary>
        public virtual void OnAttack()
        {
            TargetEntity.OnHit(Random.Range(MinDamage, MaxDamage));
        }

        /// <summary>
        /// Trigger on Animation event. Method is triggered when character is doing hit animation.
        /// </summary>
        public virtual void OnDamage()
        {
            Manager.Shake();
            _attackDamage.SetDamage(PendingDamage);
            _damageEffect.ToggleEffect(true);
            _hpHandler.AddDamage(PendingDamage);
        }

        /// <summary>
        /// Triggered on Animation Event. Method is triggered when character is doing Death Animation.
        /// </summary>
        public virtual void OnDeath() 
        {
            _hpHandler.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Triggered on Animation Event. Method is triggered when character is returning from attack
        /// </summary>
        public virtual void OnJumpBack()
        {
            State = EntityState.Returning;
            TargetPosition = Origin;
            _animator.SetInteger("Attack", 0);
            StartMove = true;
        }
        
        /// <summary>
        /// Calculate incoming damage to switch from either hit/death animation.
        /// </summary>
        /// <param name="damage"></param>
        protected virtual void OnHit(int damage)
        {
            PendingDamage = damage;
            if (_hpHandler.GetPendingDamageResult(damage) > 0)
            {
                _animator.SetTrigger("OnHit");
            }
            else
            {
                IsDead = true;
                _animator.SetTrigger("OnDeath");
            }
        }

        /// <summary>
        /// Triggered when character turns start.
        /// </summary>
        public virtual void OnTurnStart()
        {
            State = EntityState.Idle;
        }

        /// <summary>
        /// triggered when character turns end.
        /// </summary>
        public virtual void OnTurnEnd()
        {
            Manager.OnCombatContinue();
        }

        /// <summary>
        /// Set current character target 
        /// </summary>
        /// <param name="target"></param>
        public virtual void SetTarget(EntityBase target)
        {
            TargetEntity = target;
        }

        private void Update()
        {
            if (StartMove)
            {
                if(Vector3.Distance(transform.parent.position, TargetPosition) > 0.08f)
                {
                    transform.parent.position = Vector3.MoveTowards(transform.parent.position, TargetPosition , Time.deltaTime * 5f);
                }
                else
                {
                    transform.parent.position = TargetPosition;
                    StartMove = false;
                    _animator.SetTrigger("Move");
                    if (State == EntityState.Returning)
                    {
                        OnTurnEnd();
                    }
                }
            }
        }
    }
}