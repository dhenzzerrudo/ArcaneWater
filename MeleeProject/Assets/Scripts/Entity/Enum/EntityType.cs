#region File Header
// Filename: EntityType.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

namespace ArcanWater.Entity
{
    public enum EntityType
    {
        Player,
        Enemy
    }
}