#region File Header
// Filename: EntityState.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/18
// Copyright (c) 2022 Fortend
#endregion

namespace ArcanWater.Entity
{
    public enum EntityState
    {
        Idle,
        Attacking,
        Returning
    }
}