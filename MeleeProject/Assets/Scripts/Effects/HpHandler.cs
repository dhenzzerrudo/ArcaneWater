#region File Header
// Filename: HpHandlelr.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using UnityEngine;
using UnityEngine.UI;

namespace ArcaneWater.Effects
{
    public class HpHandler : MonoBehaviour
    {
        [SerializeField] 
        private Image _fillAmt;
        
        private int _max;
        private int _current;

        private float TargetFill { get; set; }


        public void Initialize(int maxHp)
        {
            _max = maxHp;
            _current = _max;
        }

        public void AddDamage(int damage)
        {
            _current -= damage;
            if (_current < 0)
            {
                _current = 0;
            }

            TargetFill = _current / (float) _max;
            _fillAmt.fillAmount = TargetFill;
        }

        public int GetPendingDamageResult(int damage)
        {
            return _current - damage;
        }
    }
}