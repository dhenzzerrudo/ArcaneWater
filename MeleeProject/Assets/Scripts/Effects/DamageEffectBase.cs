#region File Header
// Filename: DamageEffectBase.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using UnityEngine;

namespace ArcaneWater.Effects
{
    public class DamageEffectBase : MonoBehaviour
    {
        public void ShowEffect()
        {
            ToggleEffect(true);
        }

        public void HideEffect()
        {
            ToggleEffect(false);
        }

        public void ToggleEffect(bool enable)
        {
            gameObject.SetActive(enable);
        }
    }
}