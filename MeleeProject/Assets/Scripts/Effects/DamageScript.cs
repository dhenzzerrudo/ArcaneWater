#region File Header
// Filename: DamageScript.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using UnityEngine;
using UnityEngine.UI;

namespace ArcaneWater.Effects
{
    public class DamageScript : DamageEffectBase
    {
        [SerializeField]
        private Text _damageText;
        
        public void SetDamage(int damage)
        {
            _damageText.text = damage.ToString();
            ToggleEffect(true);
        }
    }
}