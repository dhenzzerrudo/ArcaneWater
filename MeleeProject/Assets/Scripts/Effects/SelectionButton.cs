#region File Header
// Filename: SelectionButton.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/18
// Copyright (c) 2022 Fortend
#endregion

using System;
using UnityEngine;
using UnityEngine.UI;

namespace ArcaneWater.Effects
{
    public class SelectionButton : MonoBehaviour
    {
        private Button _button;
        private Animator _animator;
        public Action OnButtonClicked { get; set; }

        public void Start()
        {
            _button = GetComponent<Button>();
            _animator = GetComponent<Animator>();
            _button.onClick.AddListener(HandleButtonClicked);    
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(HandleButtonClicked);
        }

        private void HandleButtonClicked()
        {
            _button.interactable = false;
            _animator.SetTrigger("Close");
        }

        public void OnClose()
        {
            OnButtonClicked?.Invoke();
            _button.interactable = true;
        }
    }
}