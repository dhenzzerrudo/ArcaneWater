#region File Header
// Filename: CameraShake.cs
// Author: Dhenz Zerrudo
// Date Created: 2022/03/17
// Copyright (c) 2022
#endregion

using System.Collections;
using UnityEngine;

namespace ArcaneWater.Effects
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] 
        private Camera _camera;

        [SerializeField] 
        private float _minPower;
        [SerializeField] 
        private float _maxPower;
        [SerializeField] 
        private float duration;

        public void Shake()
        {
            StartCoroutine(ShakeCamera(0.1f, 2F));
        }

        private IEnumerator ShakeCamera(float duration, float magnitude = 2)
        {
            var origin = transform.localPosition;
            var elapsed = 0.0f;

            while (elapsed < duration)
            {
                var posX = Random.Range(_minPower, _maxPower) * magnitude;
                var posY = Random.Range(_minPower, _maxPower) * magnitude;

                transform.localPosition = new Vector3(posX, posY, origin.z);
                elapsed += Time.deltaTime;
                yield return new WaitForSeconds(Time.deltaTime);
            }

            transform.localPosition = origin;
        }
    }
}